<?php

// Debugging //////////////////////////////////////////////////////////////////

//error_reporting(-1);
//ini_set("display_errors", 1);

// Includes ///////////////////////////////////////////////////////////////////

require_once('functions.inc.php');

// Load state /////////////////////////////////////////////////////////////////

$state = load_state();

// Database ///////////////////////////////////////////////////////////////////

$db = db_connect();

// Parameter handling /////////////////////////////////////////////////////////

if (isset($_POST['c_number'])) {
	$user_c_number = $_POST['c_number'];
} else {
	error('No answer given!');
}

// Application logic //////////////////////////////////////////////////////////

$q_number = $state['questions_answered'] + 1;
$state['questions_answered']++;

try {
	$question = get_question($db, $q_number);
	$evaluation = evaluate_answer($db, $question, $user_c_number);
} catch (Exception $e) {
	// Reset to initial state to escape the error:
	save_state(array('questions_answered' => 0,
			'questions_correct' => 0));
	error($e->getMessage());
}

if ($evaluation['correct']) {
	$state['questions_correct']++;
}

// Save state /////////////////////////////////////////////////////////////////

save_state($state);

// Redirect to feedback ///////////////////////////////////////////////////////

header("Location: answer_feedback.php?c_number=$user_c_number", true, 303);

?>
