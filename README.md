Homework 2 Sample Solution
==========================

Here's a near-ideal way to solve homework 2. I took the liberty of stealing the
questions from Robbert ^^.

Note some features of the code that make it easy to read and maintain:

* Consistent use of indentation. No mixing tabs and spaces. Same indentation
  for same level of nesting. else-blocks have the same indentation as the
  corresponding if-blocks.
* Every phase of the user interaction has its own PHP script: question, posting
  of the answer, feedback on the answer, results. This helps keep the code from
  developing into a jungle of if-then-else's.
* Posting of the answer and feedback on it are separate so as to avoid
  accidental re-posting of the same information when the page is reloaded. The
  former redirects to the latter, this is known as the post-redirect-get
  pattern. It is beyond the scope of this course, but good to know.
* All communication with the database and with cookies is done in functions in
  a separate file. This keeps the main scripts free of all the gory detail. It
  also makes it easy to replace the storage and state-keeping mechanisms
  without changing the main scripts (much).
* Complex data structures are passed between main scripts and functions as
  arrays, bundling units of information such as the application state, or
  information about the right/wrong answer. In larger projects, objects may be
  an even better alternative.
* A config.inc.sample is provided so as to document the format of the
  configuration file without leaking an actual configuration file.

Some niceties for the user:

* Use `label` HTML elements so they can click on the answers rather than having
  to hit the tiny radio buttons.

For security:

* Data that may come from an untrusted source is escaped using
  htmlspecialchars before output to prevent cross-site scripting attacks.
* Always using prepare/execute. It's not the only way to protect against SQL
  injection, but it's arguably the most readable and error-proof way.
