CREATE TABLE question (
  q_number INT NOT NULL,
  q_text VARCHAR(255),
  PRIMARY KEY (q_number)
);

CREATE TABLE choice (
  q_number INT NOT NULL,
  c_number INT NOT NULL,
  c_text VARCHAR(255),
  correct BOOLEAN NOT NULL,
  PRIMARY KEY (q_number, c_number)
);
