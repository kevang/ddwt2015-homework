INSERT INTO question(q_number, q_text) VALUES(1, "A lightsaber contains how many crystals?");
INSERT INTO question(q_number, q_text) VALUES(2, "The sith  mainly use what colour lightsaber?");
INSERT INTO question(q_number, q_text) VALUES(3, "Have you ever heard of the Millennium Falcon?");

INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(1,1,"6-8", false);
INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(1,2,"1-3", true);
INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(1,3,"4-5", false);
INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(1,4,"29-49",false);

INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(2,1,"red", true);
INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(2,2,"blue", false);
INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(2,3,"white", false);
INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(2,4,"purple", false);

INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(3,1,"What", false);
INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(3,2,"No", false);
INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(3,3,"Yes", false);
INSERT INTO choice(q_number, c_number, c_text, correct) VALUES(3,4,"Of course I have", true);
