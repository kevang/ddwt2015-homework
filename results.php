<?php

// Debugging //////////////////////////////////////////////////////////////////

//error_reporting(-1);
//ini_set("display_errors", 1);

// Includes ///////////////////////////////////////////////////////////////////

require_once('functions.inc.php');

// Load state /////////////////////////////////////////////////////////////////

$state = load_state();

// Application logic //////////////////////////////////////////////////////////

$title = 'Quiz complete';

// Reset state ////////////////////////////////////////////////////////////////

save_state(array('questions_answered' => 0,
		'questions_correct' => 0));

// Output /////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset=UTF-8 />
		<title><?=$title?></title>
		<link rel=stylesheet type=text/css href=style.css />
	</head>
	<body>
		<h1><?=$title?></h1>
		<p>You answered <?=$state['questions_correct']?> out of
		<?=$state['questions_answered']?> questions correctly.</p>
		<p><a href=.>Back to quiz</a></p>
	</body>
</html>
