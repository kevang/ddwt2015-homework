<?php

// Debugging //////////////////////////////////////////////////////////////////

//error_reporting(-1);
//ini_set("display_errors", 1);

// Includes ///////////////////////////////////////////////////////////////////

require_once('functions.inc.php');

// Load state /////////////////////////////////////////////////////////////////

$state = load_state();

// Database ///////////////////////////////////////////////////////////////////

$db = db_connect();

// Parameter handling /////////////////////////////////////////////////////////

if (isset($_GET['c_number'])) {
	$user_c_number = $_GET['c_number'];
} else {
	error('No answer given!');
}

// Application logic //////////////////////////////////////////////////////////

$q_number = $state['questions_answered'];

$title = "Answered Quiz Question $q_number";

try {
	$question = get_question($db, $q_number);
	$evaluation = evaluate_answer($db, $question, $user_c_number);
} catch (Exception $e) {
	// Reset to initial state so the user can escape the error:
	save_state(array('questions_answered' => 0,
			'questions_correct' => 0));
	error($e->getMessage());
}

// Output /////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset=UTF-8 />
		<title><?=$title?></title>
		<link rel=stylesheet type=text/css href=style.css />
	</head>
	<body>
		<h1><?=$title?></h1>
		<p><?=htmlspecialchars($question['q_text'])?></p>
		<p><b>Your answer:</b> <?=htmlspecialchars($evaluation['user_c_text'])?></p>
<?php if ($evaluation['correct']): ?>
		<p><b class=correct>Correct!</b></p>
<?php else: ?>
		<p><b class=wrong>Wrong!</b> The correct answer is:
		<?=htmlspecialchars($evaluation['correct_c_text'])?></p>
<?php endif ?>
		<p><a href=.>Continue</a></p>
	</body>
</html>
