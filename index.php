<?php

// Debugging //////////////////////////////////////////////////////////////////

//error_reporting(-1);
//ini_set("display_errors", 1);

// Includes ///////////////////////////////////////////////////////////////////

require_once('functions.inc.php');

// Load state /////////////////////////////////////////////////////////////////

$state = load_state();

// Database ///////////////////////////////////////////////////////////////////

$db = db_connect();

// Application logic //////////////////////////////////////////////////////////

$question_number = $state['questions_answered'] + 1;

$title = "Quiz Question $question_number";

try {
	$question = get_question($db, $question_number);
} catch (Exception $e) {
	// No more questions? Redirect to results.
	header('Location: results.php');
	die();
}

// Output /////////////////////////////////////////////////////////////////////
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset=UTF-8 />
		<title><?=$title?></title>
	</head>
	<body>
		<h1><?=$title?></h1>
		<form action=answer.php method=post>
			<p><?=htmlspecialchars($question['q_text'])?></p>
			<fieldset>
<?php foreach ($question['choices'] as $choice):
	$c_number = $choice['c_number'];
	$button_id = "choice_$c_number";
	$c_text = $choice['c_text'];
?>
				<input type=radio name=c_number id=<?=$button_id?> value=<?=$c_number?> />
				<label for=<?=$button_id?>><?=$c_text?></label><br />
<?php endforeach
?>
			</fieldset>
			<input type=submit value=Answer />
		</form>
	</body>
</html>
