<?php

function error($message) {
	printf('<p>ERROR: %s</p>', htmlspecialchars($message));
	printf('<p><a href=.>Back to quiz</a></p>');
	die();
}

function db_connect() {
	require_once('config.inc.php');
	return new PDO("mysql:host=$host;dbname=$dbname", $username,
			$password);
}

/**
 * Returns the application state as an array with two keys:
 * 'questions_answered' and 'questions_correct'
 */
function load_state() {
	$result = array('questions_answered' => 0,
			'questions_correct' => 0);

	if (isset($_COOKIE['questions_answered'])) {
		$result['questions_answered'] = $_COOKIE['questions_answered'];
	}

	if (isset($_COOKIE['questions_correct'])) {
		$result['questions_correct'] = $_COOKIE['questions_correct'];
	}

	return $result;
}

/**
 * Takes the application state as an array with two keys:
 * 'questions_answered' and 'questions_correct'.
 * Saves it using cookies.
 */
function save_state($state) {
	$path = pathinfo($_SERVER['PHP_SELF'])['dirname'];
	setcookie('questions_answered', $state['questions_answered'], 0
			);
	setcookie('questions_correct', $state['questions_correct'], 0);
}

/**
 * Returns an array that contains two keys:
 * 'q_text' with the text of the question with the specified $q_number.
 * 'choices' with an array with the choices.
 * Each choice is an array with the keys 'c_number', 'c_text', 'correct'.
 * Throws an exception if the question does not exist.
 */
function get_question($db, $q_number) {
	$result = array();

	$stmt = $db->prepare('SELECT q_text
			FROM question
			WHERE q_number = ?');
	$stmt->execute(array($q_number));

	if (!$stmt->rowCount()) {
		throw new Exception("No question with number $q_number exists.");
	}

	foreach ($stmt as $question) {
		$result['q_text'] = $question['q_text'];
	}

	$stmt = $db->prepare('SELECT c_number, c_text, correct
			FROM choice
			WHERE q_number = ?');
	$stmt->execute(array($q_number));

	$result['choices'] = array();

	foreach ($stmt as $choice) {
		$result['choices'][] = $choice;
	}

	return $result;
}

/**
 * Returns an array with four keys:
 * 'correct', 'user_c_text', 'correct_c_text', 'correct_c_number'
 * Throws an exception if the question or choice not exist.
 */
function evaluate_answer($db, $question, $c_number) {
	$result = array();

	foreach ($question['choices'] as $choice) {
		if ($choice['c_number'] == $c_number) {
			$result['user_c_text'] = $choice['c_text'];
		}

		if ($choice['correct']) {
			$result['correct_c_number'] = $choice['c_number'];
			$result['correct_c_text'] = $choice['c_text'];
		}
	}

	if (!isset($result['correct_c_number'])) {
		throw new Exception('There is no correct choice.');
	}

	if (!isset($result['correct_c_text'])) {
		throw new Exception('Invalid choice.');
	}

	$result['correct'] = ($c_number ==
			$result['correct_c_number']);

	return $result;
}

?>
